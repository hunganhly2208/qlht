const locale = {
	APPLICATIONS: 'Ứng dụng',
	DASHBOARDS: 'Dashboards',
	CALENDAR: 'Lịch',
	ECOMMERCE: 'E-Commerce',
	ACADEMY: 'Academy',
	MAIL: 'Mail',
	TODO: 'To-Do',
	FILE_MANAGER: 'Quản lý tệp',
	CONTACTS: 'Liên Hệ',
	CHAT: 'Trò chuyện',
	SCRUMBOARD: 'Scrumboard',
	NOTES: 'Ghi chú'
};

export default locale;
