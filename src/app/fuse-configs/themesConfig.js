import { fuseDark, skyBlue } from '@fuse/colors';
import { lightBlue, red } from '@material-ui/core/colors';

const themesConfig = {
	default: {
		palette: {
			type: 'light',
			primary: fuseDark,
			secondary: {
				light: skyBlue[100],
				main: '#089BAB',
				dark: skyBlue[900],
				contrastText: '#fff'
			},
			background: {
				paper: '#FFFFFF',
				primary: '#089BAB',
				main: '#089BAB',
				default: '#f6f7f9',
				active: 'rgba(255, 255, 255, 0.15)',
				contrastText: '#fff'
			},
			action: {
				// active: '#fff',
				// contrastText: '#fff'
			},
			text: {
				// primary: '#FFFFFF',
				// secondary: '#FFFFFF'
			},
			error: red
		},
		status: {
			danger: 'orange'
		}
	}
};

export default themesConfig;
