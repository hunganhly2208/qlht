import { authRoles } from 'app/auth';
import i18next from 'i18next';

import en from './navigation-i18n/en';
import vi from './navigation-i18n/vi';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('vi', 'navigation', vi);

const navigationConfig = [
	{
		id: 'applications',
		title: 'Applications',
		translate: 'APPLICATIONS',
		type: 'group',
		icon: 'apps',
		children: [
			{
				id: 'dashboards',
				title: 'Dashboards',
				translate: 'DASHBOARDS',
				type: 'collapse',
				icon: 'dashboard',
				children: [
					{
						id: 'analytics-dashboard',
						title: 'Analytics',
						type: 'item',
						url: '/apps/dashboards/analytics'
					},
					{
						id: 'project-dashboard',
						title: 'Project',
						type: 'item',
						url: '/apps/dashboards/project'
					}
				]
			},
			{
				id: 'e-commerce',
				title: 'E-Commerce',
				translate: 'ECOMMERCE',
				type: 'collapse',
				icon: 'shopping_cart',
				children: [
					{
						id: 'e-commerce-products',
						title: 'Products',
						type: 'item',
						url: '/apps/e-commerce/products',
						exact: true
					},
					{
						id: 'e-commerce-product-detail',
						title: 'Product Detail',
						type: 'item',
						url: '/apps/e-commerce/products/1/a-walk-amongst-friends-canvas-print',
						exact: true
					},
					{
						id: 'e-commerce-new-product',
						title: 'New Product',
						type: 'item',
						url: '/apps/e-commerce/products/new',
						exact: true
					},
					{
						id: 'e-commerce-orders',
						title: 'Orders',
						type: 'item',
						url: '/apps/e-commerce/orders',
						exact: true
					},
					{
						id: 'e-commerce-order-detail',
						title: 'Order Detail',
						type: 'item',
						url: '/apps/e-commerce/orders/1',
						exact: true
					}
				]
			}
		]
	},
	{
		id: 'user-interface',
		title: 'User Interface',
		type: 'group',
		icon: 'web',
		children: [
			{
				id: 'page-layouts',
				title: 'Page Layouts',
				type: 'collapse',
				icon: 'view_quilt',
				children: [
					{
						id: 'carded',
						title: 'Carded',
						type: 'collapse',
						badge: {
							title: 12,
							bg: '#525E8A',
							fg: '#FFFFFF'
						},
						children: [
							{
								id: 'carded-full-width',
								title: 'Full Width',
								type: 'item',
								url: '/ui/page-layouts/carded/full-width'
							},
							{
								id: 'carded-full-width-tabbed',
								title: 'Full Width Tabbed',
								type: 'item',
								url: '/ui/page-layouts/carded/full-width-tabbed'
							},
							{
								id: 'carded-full-width-2',
								title: 'Full Width 2',
								type: 'item',
								url: '/ui/page-layouts/carded/full-width-2'
							},
							{
								id: 'carded-full-width-2-tabbed',
								title: 'Full Width 2 Tabbed',
								type: 'item',
								url: '/ui/page-layouts/carded/full-width-2-tabbed'
							},
							{
								id: 'carded-left-sidebar',
								title: 'Left Sidebar',
								type: 'item',
								url: '/ui/page-layouts/carded/left-sidebar'
							},
							{
								id: 'carded-left-sidebar-tabbed',
								title: 'Left Sidebar Tabbed',
								type: 'item',
								url: '/ui/page-layouts/carded/left-sidebar-tabbed'
							},
							{
								id: 'carded-left-sidebar-2',
								title: 'Left Sidebar 2',
								type: 'item',
								url: '/ui/page-layouts/carded/left-sidebar-2'
							},
							{
								id: 'carded-left-sidebar-2-tabbed',
								title: 'Left Sidebar 2 Tabbed',
								type: 'item',
								url: '/ui/page-layouts/carded/left-sidebar-2-tabbed'
							},
							{
								id: 'carded-right-sidebar',
								title: 'Right Sidebar',
								type: 'item',
								url: '/ui/page-layouts/carded/right-sidebar'
							},
							{
								id: 'carded-right-sidebar-tabbed',
								title: 'Right Sidebar Tabbed',
								type: 'item',
								url: '/ui/page-layouts/carded/right-sidebar-tabbed'
							},
							{
								id: 'carded-right-sidebar-2',
								title: 'Right Sidebar 2',
								type: 'item',
								url: '/ui/page-layouts/carded/right-sidebar-2'
							},
							{
								id: 'carded-right-sidebar-2-tabbed',
								title: 'Right Sidebar 2 Tabbed',
								type: 'item',
								url: '/ui/page-layouts/carded/right-sidebar-2-tabbed'
							}
						]
					},
					{
						id: 'simple',
						title: 'Simple',
						type: 'collapse',
						badge: {
							title: 8,
							bg: '#525E8A',
							fg: '#FFFFFF'
						},
						children: [
							{
								id: 'simple-full-width',
								title: 'Full Width',
								type: 'item',
								url: '/ui/page-layouts/simple/full-width'
							},
							{
								id: 'simple-left-sidebar',
								title: 'Left Sidebar',
								type: 'item',
								url: '/ui/page-layouts/simple/left-sidebar'
							},
							{
								id: 'simple-left-sidebar-2',
								title: 'Left Sidebar 2',
								type: 'item',
								url: '/ui/page-layouts/simple/left-sidebar-2'
							},
							{
								id: 'simple-left-sidebar-3',
								title: 'Left Sidebar 3',
								type: 'item',
								url: '/ui/page-layouts/simple/left-sidebar-3'
							},
							{
								id: 'simple-right-sidebar',
								title: 'Right Sidebar',
								type: 'item',
								url: '/ui/page-layouts/simple/right-sidebar'
							},
							{
								id: 'simple-right-sidebar-2',
								title: 'Right Sidebar 2',
								type: 'item',
								url: '/ui/page-layouts/simple/right-sidebar-2'
							},
							{
								id: 'simple-right-sidebar-3',
								title: 'Right Sidebar 3',
								type: 'item',
								url: '/ui/page-layouts/simple/right-sidebar-3'
							},
							{
								id: 'simple-tabbed',
								title: 'Tabbed',
								type: 'item',
								url: '/ui/page-layouts/simple/tabbed'
							}
						]
					},
					{
						id: 'blank',
						title: 'Blank',
						type: 'item',
						url: '/ui/page-layouts/blank'
					}
				]
			}
		]
	}
];

export default navigationConfig;
