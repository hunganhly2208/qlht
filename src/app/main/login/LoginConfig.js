import { authRoles } from 'app/auth';
import Login from './Login';
import i18next from 'i18next';

import en from './login-i18n/en';
import vi from './login-i18n/vi';

i18next.addResourceBundle('vi', 'login', vi);
i18next.addResourceBundle('en', 'login', en);
const LoginConfig = {
	settings: {
		layout: {
			config: {
				navbar: {
					display: false
				},
				toolbar: {
					display: false
				},
				footer: {
					display: false
				},
				leftSidePanel: {
					display: false
				},
				rightSidePanel: {
					display: false
				}
			}
		}
	},
	auth: authRoles.onlyGuest,
	routes: [
		{
			path: '/login',
			component: Login
		}
	]
};

export default LoginConfig;
