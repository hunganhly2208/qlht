const locale = {
	DO_NOT_HAVE_AN_ACCOUNT: "Don't have an account?",
	REGISTER: "Register",
	LOGIN: "Login",
}
export default locale;
