const locale = {
	DO_NOT_HAVE_AN_ACCOUNT: "Bạn chưa có tài khoản?",
	REGISTER: "Đăng ký",
	LOGIN: "Đăng nhập"
};

export default locale;
