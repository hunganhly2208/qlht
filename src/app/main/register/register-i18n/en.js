const locale = {
	ALREADY_HAVE_AN_ACCOUNT: "Already have an account?",
	REGISTER: "Register",
	LOGIN: "Login"
}
export default locale;
