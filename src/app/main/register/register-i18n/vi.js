const locale = {
	ALREADY_HAVE_AN_ACCOUNT: "Bạn đã có tài khoản?",
	REGISTER: "Đăng ký",
	LOGIN: "Đăng nhập"
};

export default locale;
