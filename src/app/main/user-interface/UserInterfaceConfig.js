import React from 'react';
import cardedLayoutRoutes from './page-layouts/carded/cardedLayoutRoutes';
import simpleLayoutRoutes from './page-layouts/simple/simpleLayoutRoutes';

const UserInterfaceConfig = {
	routes: [
		...cardedLayoutRoutes,
		...simpleLayoutRoutes,
		{
			path: '/ui/page-layouts/blank',
			component: React.lazy(() => import('./page-layouts/blank'))
		}
	]
};

export default UserInterfaceConfig;
