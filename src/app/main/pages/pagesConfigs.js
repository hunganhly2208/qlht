import ProfilePageConfig from './profile/ProfilePageConfig';
import ComingSoonPageConfig from './coming-soon/ComingSoonPageConfig';
import Error404PageConfig from './errors/404/Error404PageConfig';
import Error500PageConfig from './errors/500/Error500PageConfig';

const pagesConfigs = [ComingSoonPageConfig, Error404PageConfig, Error500PageConfig, ProfilePageConfig];

export default pagesConfigs;
