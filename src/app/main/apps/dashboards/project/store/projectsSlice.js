import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { api } from 'app/store/axiosInstance';

export const getProjects = createAsyncThunk('projectDashboardApp/projects/getProjects', async () => {
	const response = await api.get('/api/project-dashboard-app/projects');
	return response.data;
});

const projectsAdapter = createEntityAdapter({});

export const {
	selectAll: selectProjects,
	selectEntities: selectProjectsEntities,
	selectById: selectProjectById
} = projectsAdapter.getSelectors(state => state.projectDashboardApp.projects);

const projectsSlice = createSlice({
	name: 'projectDashboardApp/projects',
	initialState: projectsAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getProjects.fulfilled]: projectsAdapter.setAll
	}
});

export default projectsSlice.reducer;
