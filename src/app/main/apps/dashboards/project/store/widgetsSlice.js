import { createEntityAdapter, createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { api } from 'app/store/axiosInstance';

export const getWidgets = createAsyncThunk('projectDashboardApp/widgets/getWidgets', async () => {
	const response = await api.get('/api/project-dashboard-app/widgets');
	const data = await response.data;

	return data;
});

const widgetsAdapter = createEntityAdapter({});

export const { selectEntities: selectWidgets, selectById: selectWidgetById } = widgetsAdapter.getSelectors(
	state => state.projectDashboardApp.widgets
);

const widgetsSlice = createSlice({
	name: 'projectDashboardApp/widgets',
	initialState: widgetsAdapter.getInitialState(),
	reducers: {},
	extraReducers: {
		[getWidgets.fulfilled]: widgetsAdapter.setAll
	}
});

export default widgetsSlice.reducer;
