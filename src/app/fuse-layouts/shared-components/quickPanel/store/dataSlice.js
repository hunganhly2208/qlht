import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { api } from 'app/store/axiosInstance';

export const getData = createAsyncThunk('quickPanel/data/getData', async () => {
	const response = await api.get('/api/quick-panel/data');
	const data = await response.data;

	return data;
});

const dataSlice = createSlice({
	name: 'quickPanel/data',
	initialState: null,
	reducers: {},
	extraReducers: {
		[getData.fulfilled]: (state, action) => action.payload
	}
});

export default dataSlice.reducer;
